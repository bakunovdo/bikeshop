import ReactDOM from "react-dom";
import React from "react";

import { Application } from "app/Application";

ReactDOM.render(
  <React.StrictMode>
    <Application />
  </React.StrictMode>,
  document.getElementById("root")
);
