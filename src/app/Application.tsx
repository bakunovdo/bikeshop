import { Router } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import { createBrowserHistory } from "history";
import { ThemeProvider } from "@emotion/react";

import { routes } from "pages";
import { theme } from "app/theme";

import "normalize.css";
import "./index.css";

const browserHistory = createBrowserHistory();

export const Application: React.FC = () => {
  return (
    <Router history={browserHistory}>
      <ThemeProvider theme={theme}>{renderRoutes(routes())}</ThemeProvider>
    </Router>
  );
};
