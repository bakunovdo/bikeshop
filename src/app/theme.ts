export const textVariants = {
  xsmall: {
    fontSize: "0.75rem",
  },
  small: {
    fontSize: "0.875rem",
  },
  medium: {
    fontSize: "1rem",
  },
  large: {
    fontSize: "1.125rem",
  },
};

export const theme = {
  colors: {
    base: "#fff",
    gold: "gold",
  },
  fonts: {
    text: ["FuturaPT", "-apple-system", '"Segoe UI"', "Roboto", "Helvetica Neue"].join(", "),
  },
  textVariants,
};

export type TTheme = typeof theme;
