import { Home } from "./home";

const exactRoute = (
  path: string,
  component: React.ComponentType,
  additionalFields?: Record<string, unknown>
) => ({
  exact: true,
  path,
  component,
  ...additionalFields,
});

export const routes = () => [exactRoute("/", Home)];
