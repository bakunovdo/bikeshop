import { Hero } from "./sections/hero";

import { Box } from "components/atoms";

export const Home: React.FC = () => {
  return (
    <Box>
      <Hero />
    </Box>
  );
};
