import styled from "@emotion/styled";

import { HeaderBar } from "components/organisms/HeaderBar";
import { Box } from "components/atoms";
import bgImage from "assets/fullscreen/bechir.png";

export const Hero: React.FC = () => {
  return (
    <BaseLayoutStyled>
      <HeaderBar />
    </BaseLayoutStyled>
  );
};

const BaseLayoutStyled = styled(Box)`
  background: linear-gradient(180deg, rgba(0, 0, 0, 0.46) 0%, rgba(0, 0, 0, 0) 21.05%),
    linear-gradient(0deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),
    url(${bgImage}) no-repeat center top;
  background-size: cover;
  height: 100vh;
  padding: 2rem;
`;
