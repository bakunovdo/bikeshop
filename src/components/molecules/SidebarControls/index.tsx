import { CartButton } from "./cart-button";
import { BurgerButton } from "./burger-button";

import { Flex } from "components/atoms";

export const SidebarControls: React.FC = () => {
  return (
    <Flex alignItems="center">
      <CartButton />
      <BurgerButton />
    </Flex>
  );
};
