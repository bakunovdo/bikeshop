import { useState } from "react";
import styled from "@emotion/styled";

export const BurgerButton = () => {
  const [open, setOpen] = useState(false);
  const toggleOpen = () => setOpen(!open);

  return (
    <Container className={open ? "open" : ""} onClick={toggleOpen}>
      <Line />
      <Line />
      <Line />
    </Container>
  );
};

const Container = styled.div`
  margin-left: 2rem;
  position: relative;
  width: 20px;
  height: 20px;
  transform: rotate(0deg);
  transition: 0.5s ease-in-out;
  cursor: pointer;

  span:nth-child(1) {
    top: 0;
    transform-origin: left center;
  }

  span:nth-child(2) {
    top: 9px;
    transform-origin: left center;
  }

  span:nth-child(3) {
    top: 18px;
    transform-origin: left center;
  }

  &.open span:nth-child(1) {
    transform: rotate(45deg);
    top: 2px;
    left: 3px;
  }

  &.open span:nth-child(2) {
    width: 0%;
    opacity: 0;
  }

  &.open span:nth-child(3) {
    transform: rotate(-45deg);
    top: 16px;
    left: 3px;
  }
`;

const Line = styled.span`
  display: block;
  position: absolute;
  height: 2px;
  width: 100%;
  background: #fff;
  border-radius: 9px;
  opacity: 1;
  left: 0;
  transform: rotate(0deg);
  transition: 0.25s ease-in-out;
`;
