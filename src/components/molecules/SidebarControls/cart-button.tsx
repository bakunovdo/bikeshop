import { Link } from "react-router-dom";

import cartImage from "assets/icons/cart.svg";

export const CartButton = () => {
  return (
    <Link to="/">
      <img src={cartImage} alt="cart-icon" />
    </Link>
  );
};
