import styled from "@emotion/styled";

import { Flex } from "components/atoms/Layout";
import { Input } from "components/atoms";
import searchSvg from "assets/icons/search.svg";

export const InputSearch: React.FC = () => {
  return (
    <Container position="relative">
      <Icon src={searchSvg} alt="search-icon" />
      <InputStyled />
    </Container>
  );
};

const InputStyled = styled(Input)`
  border: none;
  border-radius: 15.5px;
  padding: 0.25rem 1rem 0.25rem 2.25rem;
  height: 30px;
  width: 100%;
`;

const Icon = styled.img`
  position: absolute;
  left: 1rem;
`;

const Container = styled(Flex)`
  align-items: center;
  width: 400px;
`;
