import { SidebarControls } from "components/molecules/SidebarControls";
import { InputSearch } from "components/molecules";
import { Logo } from "components/atoms/Logo";
import { Flex } from "components/atoms";

export const HeaderBar: React.FC = () => {
  return (
    <Flex justifyContent="space-between" alignItems="center">
      <Logo />
      <InputSearch />
      <SidebarControls />
    </Flex>
  );
};
