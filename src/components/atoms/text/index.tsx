import {
  color,
  ColorProps,
  layout,
  LayoutProps,
  space,
  SpaceProps,
  typography,
  TypographyProps,
  variant,
} from "styled-system";
import styled from "@emotion/styled";

import { textVariants } from "app/theme";

type CustomProps = {
  size: keyof typeof textVariants;
};

type StyledSystemProps = SpaceProps & TypographyProps & LayoutProps & ColorProps;
type TextProps = CustomProps & StyledSystemProps;

const sizeVariants = variant({
  prop: "size",
  variants: textVariants,
});

export const Text = styled.p<TextProps>`
  font-family: ${({ theme }) => theme.fonts.text};

  ${space};
  ${typography};
  ${layout};
  ${color};
  ${sizeVariants};
`;

Text.defaultProps = {
  size: "medium",
};
