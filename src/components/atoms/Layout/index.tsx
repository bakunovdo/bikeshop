import {
  flexbox,
  FlexboxProps,
  layout,
  LayoutProps,
  position,
  PositionProps,
  space,
  SpaceProps,
} from "styled-system";
import styled from "@emotion/styled";

type TBox = LayoutProps & SpaceProps & PositionProps;

export const Box = styled.div<TBox>`
  ${layout};
  ${space};
  ${position};
`;

export const Flex = styled(Box)<FlexboxProps>`
  display: flex;
  ${flexbox};
`;

export const Column = styled(Flex)`
  flex-direction: column;
`;

export const Container = styled(Box)`
  max-width: 1300px;
`;
