import { FC, InputHTMLAttributes } from "react";
import styled from "@emotion/styled";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  type?: string;
};

export const Input: FC<InputProps> = ({ type, ...rest }) => {
  return <InputStyled type={type} {...rest} />;
};

Input.defaultProps = {
  type: "text",
};

const InputStyled = styled.input`
  font-family: ${({ theme }) => theme.fonts.text};
  ${({ theme }) => theme.textVariants.medium}

  background: #ffffff;
  border: 3px solid #000;
  outline: none;
`;
