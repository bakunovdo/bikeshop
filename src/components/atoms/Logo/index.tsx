import { Link } from "react-router-dom";

import logoImage from "assets/logo.png";

export const Logo: React.FC = () => {
  return (
    <Link to="/">
      <img src={logoImage} alt="site-logo" />
    </Link>
  );
};
