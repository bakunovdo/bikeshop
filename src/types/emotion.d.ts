import "@emotion/react";
import { TTheme } from "../app/theme";

declare module "@emotion/react" {
  export interface Theme extends TTheme {}
}
